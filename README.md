# App localisation #

This repository contains the latest masterfile for the Conneqtech AXA IN application.

## How to use this ##
- Make a fork of this repository or download the masterfile
- Changes the keys you would like to be different in your app flavour
- Send only the keys you changed to iot.support@conneqtech.com, making sure you translated your custom keys to all languages you want to support